.include "m2560def.inc"

start:
.dseg

	a: .byte 5

.cseg
	ldi ZH, high(b<<1)
	ldi ZL, low(b<<1)
	lpm r16, Z+
	lpm r17, Z+
	lpm r18, Z+
	lpm r19, Z+
	lpm r20, Z+

	ldi ZH, high(c<<1)
	ldi ZL, low(c<<1)
	lpm r21, Z+
	lpm r22, Z+
	lpm r23, Z+
	lpm r24, Z+
	lpm r25, Z+

	add r16, r21
	add r17, r22
	add r18, r23
	add r19, r24
	add r20, r25

	ldi ZH, high(a)
	ldi ZL, low(a)
	st Z+, r16
	st Z+, r17
	st Z+, r18
	st Z+, r19
	st Z+, r20
	

halt:
jmp halt

	b: .db 1,2,3,4,5
	c: .db 5,4,3,2,1
