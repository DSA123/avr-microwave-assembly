.include "m2560def.inc"
.def counter = r16
.def counter2 = r17
.def counter3 = r18
.def temp1 = r20
.def temp2 = r21
.def current = r23

.macro do_lcd_command
	ldi temp1, @0
	 
	rcall lcd_command
	rcall lcd_wait
.endmacro

.macro do_lcd_data
	mov temp1, @0
	ori temp1, 0x30
	rcall lcd_data
	rcall lcd_wait
.endmacro

.macro clear
ldi YL, low(@0)
; load the memory address to Y
ldi YH, high(@0)
clr temp2
st Y+, temp2
; clear the two bytes at @0 in SRAM
st Y, temp2
.endmacro

.dseg
SecondCounter:
.byte 2
; Two-byte counter for counting seconds.
TempCounter:
.byte 2 
.cseg
.org 0
	jmp RESET
.org INT2addr
	jmp Hole
.org OVF0addr
	jmp Timer0OVF

RESET:
	ldi	temp1, low(RAMEND) ; initialize the stack
	out SPL, temp1
	ldi temp1, high(RAMEND)
	out SPH, temp1
	ser temp1
	out DDRF, temp1
	out DDRA, temp1
	clr temp1
	out PORTF, temp1
	out PORTA, temp1
	do_lcd_command 0b00111000 ; 2x5x7
	rcall sleep_5ms
	do_lcd_command 0b00111000 ; 2x5x7
	rcall sleep_1ms
	do_lcd_command 0b00111000 ; 2x5x7
	do_lcd_command 0b00111000 ; 2x5x7
	do_lcd_command 0b00001000 ; display off?
	do_lcd_command 0b00000001 ; clear display
	do_lcd_command 0b00000110 ; increment, no display shift
	do_lcd_command 0b00001110 ; Cursor on, bar, no blink
	;enables induvidual interupts for button 0 & 1
	ldi temp1, (0b10<<ISC20)
	sts EICRA, temp1
	ldi temp1, (1<<INT2)
	out EIMSK, temp1
	clr counter
	clr counter2
clear TempCounter
; Initialize the temporary counter to 0
clear SecondCounter
; Initialize the second counter to 0
	ldi temp1, 0b00000000
	out TCCR0A, temp1
	ldi temp1, 0b00000010
	out TCCR0B, temp1
	;Prescaling	value=8
	ldi	temp1, 1<<TOIE0
	; = 128 microseconds
	sts TIMSK0, temp1
	; T/C0 interrupt enable
	sei

Halt:
	jmp Halt


Hole:
inc counter
cpi counter, 4
brne skip
clr counter
inc counter2
skip:
reti


Timer0OVF:
; Load the value of the temporary counter.
lds r24, TempCounter
;increments the pointer to temp counter to get the next byte
lds r25, TempCounter+1

;incriments r24 and r25 by 1
adiw r25:r24, 1
cpi r24, low(3906)
; Check if (r25:r24) = 7812
ldi temp2, high(3906)
cpc r25, temp2
brne NotHalfSecond
do_lcd_command 0b00000001 ; clear display
lsl counter2 ;multiple by 2 to make it the value for rps (rather than the value per 500ms)
mov current, counter2
clr temp2
jmp find_hundreds
printFinish:
clr counter
clr counter2
clearCounter:
clear TempCounter
; Reset the temporary counter.
; Load the value of the second counter.
lds r24, SecondCounter
lds r25, SecondCounter+1
adiw r25:r24, 1
; Increase the second counter by one.
; continued
sts SecondCounter, r24
sts SecondCounter+1, r25
rjmp EndIF
NotHalfSecond:
; Store the new value of the temporary counter.
sts TempCounter, r24
sts TempCounter+1, r25
EndIF:
reti
; Return from the interrupt.




find_hundreds:
	cpi current, 100
	brlo print_hundreds
	subi current, 100
	inc temp2
	jmp find_hundreds
print_hundreds:
	do_lcd_data temp2
	clr temp2
find_tens:	
	cpi current,10
	brlo find_ones
	inc temp2
	subi current, 10
	jmp find_tens
find_ones:
	do_lcd_data temp2
	clr temp2
	do_lcd_data current
	jmp printFinish



.equ LCD_RS = 7
.equ LCD_E = 6
.equ LCD_RW = 5
.equ LCD_BE = 4

.macro lcd_set
	sbi PORTA, @0
.endmacro
.macro lcd_clr
	cbi PORTA, @0
.endmacro

;
; Send a command to the LCD (r16)
;

lcd_command:
	out PORTF, temp1
	rcall sleep_1ms
	lcd_set LCD_E
	rcall sleep_1ms
	lcd_clr LCD_E
	rcall sleep_1ms
	ret

lcd_data:
	out PORTF, temp1
	lcd_set LCD_RS
	rcall sleep_1ms
	lcd_set LCD_E
	rcall sleep_1ms
	lcd_clr LCD_E
	rcall sleep_1ms
	lcd_clr LCD_RS
	ret

lcd_wait:
	push temp1
	clr temp1
	out DDRF, temp1
	out PORTF, temp1
	lcd_set LCD_RW
lcd_wait_loop:
	rcall sleep_1ms
	lcd_set LCD_E
	rcall sleep_1ms
	in temp1, PINF
	lcd_clr LCD_E
	sbrc temp1, 7
	rjmp lcd_wait_loop
	lcd_clr LCD_RW
	ser temp1
	out DDRF, temp1
	pop temp1
	ret

.equ F_CPU = 16000000
.equ DELAY_1MS = F_CPU / 4 / 1000 - 4
; 4 cycles per iteration - setup/call-return overhead

sleep_1ms:
	push r31
	push r30
	ldi r31, high(DELAY_1MS)
	ldi r30, low(DELAY_1MS)
delayloop_1ms:
	sbiw r31:r30, 1
	brne delayloop_1ms
	pop r31
	pop r30
	ret

sleep_5ms:
	rcall sleep_1ms
	rcall sleep_1ms
	rcall sleep_1ms
	rcall sleep_1ms
	rcall sleep_1ms
	ret
