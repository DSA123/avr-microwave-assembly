.include "m2560def.inc"

.dseg
	a:	.byte 100
.cseg
start:
	ldi ZH, HIGH(d<<1)
	ldi ZL, LOW(d<<1)
	ldi XH, HIGH(a)
	ldi XL, LOW(a)
	ldi r19, 0x01
loop:
	add r16, r19
	lpm r17, Z+
	ldi r18, 'e'
	st X+, r16
	cpi r17, 0x0
	breq notFound
	cp r17, r18
	brne loop

	

halt:
	jmp halt
notFound:
	ldi r16, 0xFF
	jmp halt
d: .db 'J','a','k','e',' ','i','s',' ','#','1',0
