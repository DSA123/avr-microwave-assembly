.include "m2560def.inc"

start:
.dseg

	a: .byte 10

.cseg
	ldi ZH, high(b<<1)
	ldi ZL, low(b<<1)
	lpm r16, Z+
	lpm r17, Z+
	lpm r18, Z+
	lpm r19, Z+
	lpm r20, Z+
	lpm r21, Z+
	lpm r22, Z+
restore:
	ldi ZH, high(a)
	ldi ZL, low(a)
	st Z+, r16
	st Z+, r17
	st Z+, r18
	st Z+, r19
	st Z+, r20
	st Z+, r21
	st Z+, r22

loop:
	ldi r24, 0x0
	cp r16, r17
	breq second
	brlt second
	ldi r24, 0x01
	mov r23, r16
	mov r16, r17
	mov r17, r23
second:
	cp r17, r18
	breq third
	brlt third
	ldi r24, 0x01
	mov r23, r17
	mov r17, r18
	mov r18, r23
third:
	cp r18, r19
	breq fourth
	brlt fourth
	ldi r24, 0x01
	mov r23, r18
	mov r18, r19
	mov r19, r23
fourth:
	cp r19, r20
	breq fifth
	brlt fifth
	ldi r24, 0x01
	mov r23, r19
	mov r19, r20
	mov r20, r23
fifth:
	cp r20, r21
	breq sixth
	brlt sixth
	ldi r24, 0x01
	mov r23, r20
	mov r20, r21
	mov r21, r23
sixth:
	cp r21, r22
	breq shouldLoop
	brlt shouldLoop
	ldi r24, 0x01
	mov r23, r21
	mov r21, r22
	mov r22, r23
shouldLoop:
	cpi r24, 0x01
	breq loop
	ldi ZH, high(a)
	ldi ZL, low(a)
	st Z+, r16
	st Z+, r17
	st Z+, r18
	st Z+, r19
	st Z+, r20
	st Z+, r21
	st Z+, r22
halt:
jmp halt

	b: .db 7, 4, 5, 1, 6, 3, 2
