.include "m2560def.inc"

start:
	ldi r17, HIGH(40960)
	ldi r16, LOW(40960)
	ldi r19, HIGH(2730)
	ldi r18, LOW(2730)
	add r18, r16
	adc r19, r17
	mov r20, r18
	mov r21, r19

halt:
	jmp halt

