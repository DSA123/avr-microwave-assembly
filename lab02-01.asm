.include "m2560def.inc"

.dseg
	a:	.byte 100
.cseg
start:
	ldi ZH, HIGH(b<<1)
	ldi ZL, LOW(b<<1)
	ldi YH, HIGH(a)
	ldi YL, LOW(a)

	ldi XL, LOW(RAMEND)
	ldi XH, HIGH(RAMEND)
	out SPH, XH
	out SPL, XL
	push r20
	notEnd:
		lpm r16, Z+
		cp r20, r16
		brge notEndData
		push r16
		jmp notEnd
	notEndData:
		pop r16
		st Y+, r16
		ldi r21, 0x1
		cp r16, r21
		brge notEndData

	halt:
		jmp halt

b: .db 'a', 'b', 'c',0
