.include "m2560def.inc"

.dseg
	a:	.byte 100
.cseg
start:
	ldi ZH, HIGH(d<<1)
	ldi ZL, LOW(d<<1)
	ldi XH, HIGH(a)
	ldi XL, LOW(a)
	ldi r17, 0x20
loop:
	lpm r16, Z+
	; if between 0x61, 0x7a change to upper by subtracting 0x20
	cpi r16, 0x61
	brlt notlow
	cpi r16, 0x7b
	brge notlow
	sub r16, r17
notlow:
	st X+, r16
	cpi r16, 0x0
	brne loop
	
halt:
	jmp halt

d: .db 'J','a','k','e',' ','i','s',' ','#','1',0
