.include "m2560def.inc"

.def row = r16 ; current row number
.def col = r17 ; current column number
.def rmask = r18 ; mask for current row during scan
.def cmask = r19 ; mask for current column during scan
.def temp1 = r20
.def temp2 = r21
.def pressFlag = r22
.def mm = r23
.def ss = r24
.def toPrint = r25
.def read = r26
.def mul10 = r29 

.equ PORTLDIR = 0xF0 ; PD7-4: output, PD3-0, input
.equ INITCOLMASK = 0xEF ; scan from the rightmost column,
.equ INITROWMASK = 0x01 ; scan from the top row
.equ ROWMASK = 0x0F ; for obtaining input from Port D

.macro do_lcd_command
	ldi temp1, @0
	rcall lcd_command
	rcall lcd_wait
.endmacro

.macro do_lcd_data
	mov temp1, @0
	ori temp1, 0x30
	rcall lcd_data
	rcall lcd_wait
.endmacro

.cseg
.org 0x0000
	jmp RESET

RESET:
	ldi	temp1, low(RAMEND) ; initialize the stack
	out SPL, temp1
	ldi temp1, high(RAMEND)
	out SPH, temp1
	
	ldi temp1, PORTLDIR ; PA7:4/PA3:0, out/in
	sts DDRL, temp1

	ser temp1
	out DDRF, temp1
	out DDRA, temp1
	out DDRC, temp1
	clr temp1
	out PORTF, temp1
	out PORTA, temp1
	do_lcd_command 0b00111000 ; 2x5x7
	rcall sleep_5ms
	do_lcd_command 0b00111000 ; 2x5x7
	rcall sleep_1ms
	do_lcd_command 0b00111000 ; 2x5x7
	do_lcd_command 0b00111000 ; 2x5x7
	do_lcd_command 0b00001000 ; display off?
	do_lcd_command 0b00000001 ; clear display
	do_lcd_command 0b00000110 ; increment, no display shift
	do_lcd_command 0b00001110 ; Cursor on, bar, no blink
	clr pressFlag

clearTime:
	clr read
	clr mm
	clr ss
	out PORTC, read
	
entryMode:
	do_lcd_command 0b00000001
	rcall printTime
	jmp createmask

main:
	clr pressFlag

createmask:
	ldi	cmask, INITCOLMASK ; initial column mask
	clr col ; initial column
colloop:
	cpi col, 4
	breq main ; If all keys are scanned, repeat.
	sts PORTL, cmask ; Otherwise, scan a column.

	ldi temp1, 0xFF ; Slow down the scan operation.
delay:
	dec temp1
	brne delay

	lds temp1, PINL ; Read PORTA
	andi temp1, ROWMASK ; Get the keypad output value
	cpi temp1, 0xF ; Check if any row is low
	breq nextcol ; If yes, find which row is low

	ldi rmask, INITROWMASK ; Initialize for row check
	clr row
 
rowloop:
	cpi row, 4
	breq nextcol ; the row scan is over.
	mov temp2, temp1
	and temp2, rmask ; check un - masked bit
	breq convert ; if bit is clear, the key is pressed
	inc row ; else move to the next row
	lsl rmask
	jmp rowloop

nextcol:    ; if row scan is over
	lsl cmask
	inc col ; increase column value
	jmp colloop ; go to the next column

convert:
	cpi pressFlag, 1
	breq createmask
	ldi pressFlag, 1
	cpi col, 3 ; If the pressed key is in col.3
	breq letters ; we have a letter

			 ; If the key is not in col.3 and

	cpi row, 3 ; If the key is in row3,
	breq symbols ; we have a symbol or 0

	mov temp1, row ; Otherwise we have a number in 1-9
	lsl temp1
	add temp1, row
	add temp1, col ; temp1 = row*3 + col
	subi temp1, -1 ; Add the value of character �1�
	jmp numbers

letters:
	ldi temp1, 'A'
	add temp1, row ; Get the ASCII value for the key
	cpi temp1, 'A'
	breq A
	cpi temp1, 'B'
	breq B
	cpi temp1, 'C'
	breq C
	cpi temp1, 'D'
	breq D
A:
//

B:
//

C:
//

D:
//

symbols:
	cpi col, 0 ; Check if we have a star
	breq star
	cpi col, 1 ; or if we have zero
	breq zero
	ldi temp1, '#' ; if not we have hash
	breq hash

star:
/*
	ldi temp1, '*' ; Set to star
	clr result
	clr current
	do_lcd_command 0b00000001
	do_lcd_data result
	do_lcd_command 0b11000000
	jmp main
*/

hash:
//

zero:
/*
	ldi temp1, 0 ; Set to zero
	ldi temp2, 10
	mul current, temp2
	mov current, r0
	add current, temp1
	do_lcd_data temp1
	jmp createmask
*/

	clr temp1
/*
convert_end:

	clr temp2
	mov current, result
	//mov temp1, current
	do_lcd_command 0b00000001
	do_lcd_command 0b10000000
	jmp find_hundreds
	do_lcd_command 0b11000000
	clr current
	jmp createmask
	*/
numbers:	
	cpi read, 4
	brlo cont
	jmp createmask
cont:
	inc read
	out PORTC, read
	ldi mul10, 10
	mul mm, mul10
	mov mm, r0
	clr temp2

findHighSec:
	cpi ss, 10
	brlo MinAdd
	subi ss, 10
	inc temp2
	jmp findHighSec

MinAdd:
	add mm, temp2
	mul ss, mul10
	mov ss, r0
	add ss, temp1
	mov r28, mm
	jmp entryMode

printTime:
	push temp1
	push temp2
	clr temp2
	mov toPrint, mm
	rcall find_hundreds
	ldi temp1, ':'
	do_lcd_data temp1
	pop temp2
	pop temp1
	mov toPrint, ss
	push temp1
	push temp2
	clr temp2
	rcall find_hundreds
	pop temp2
	pop temp1
	ret

find_hundreds:
	cpi toPrint, 100
	brlo print_hundreds
	subi toPrint, 100
	inc temp2
	jmp find_hundreds
print_hundreds:
	do_lcd_data temp2
	clr temp2
find_tens:
	cpi toPrint,10
	brlo find_ones
	inc temp2
	subi toPrint, 10
	jmp find_tens
find_ones:
	do_lcd_data temp2
	clr temp2
	do_lcd_data toPrint
	ret

Halt:
	jmp Halt
.equ LCD_RS = 7
.equ LCD_E = 6
.equ LCD_RW = 5
.equ LCD_BE = 4

.macro lcd_set
	sbi PORTA, @0
.endmacro
.macro lcd_clr
	cbi PORTA, @0
.endmacro

;
; Send a command to the LCD (r16)
;

lcd_command:
	out PORTF, temp1
	rcall sleep_1ms
	lcd_set LCD_E
	rcall sleep_1ms
	lcd_clr LCD_E
	rcall sleep_1ms
	ret

lcd_data:
	out PORTF, temp1
	lcd_set LCD_RS
	rcall sleep_1ms
	lcd_set LCD_E
	rcall sleep_1ms
	lcd_clr LCD_E
	rcall sleep_1ms
	lcd_clr LCD_RS
	ret

lcd_wait:
	push temp1
	clr temp1
	out DDRF, temp1
	out PORTF, temp1
	lcd_set LCD_RW
lcd_wait_loop:
	rcall sleep_1ms
	lcd_set LCD_E
	rcall sleep_1ms
	in temp1, PINF
	lcd_clr LCD_E
	sbrc temp1, 7
	rjmp lcd_wait_loop
	lcd_clr LCD_RW
	ser temp1
	out DDRF, temp1
	pop temp1
	ret

.equ F_CPU = 16000000
.equ DELAY_1MS = F_CPU / 4 / 1000 - 4
; 4 cycles per iteration - setup/call-return overhead

sleep_1ms:
	push r31
	push r30
	ldi r31, high(DELAY_1MS)
	ldi r30, low(DELAY_1MS)
delayloop_1ms:
	sbiw r31:r30, 1
	brne delayloop_1ms
	pop r31
	pop r30
	ret

sleep_5ms:
	rcall sleep_1ms
	rcall sleep_1ms
	rcall sleep_1ms
	rcall sleep_1ms
	rcall sleep_1ms
	ret

