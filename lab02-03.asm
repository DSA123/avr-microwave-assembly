.include "m2560def.inc"

.set NEXT_INT = 0x0000
.macro defint ; str
	.set T = PC
	.dw NEXT_INT << 1
	.set NEXT_INT = T
	.db @0, 0
.endmacro


.cseg

jmp main
	defint 1
	defint 2
	defint 3

main:
	ldi ZL, LOW(NEXT_INT <<1)
	ldi ZH, HIGH(NEXT_INT <<1)
	rcall finder
	rjmp end
finder:
	cpi ZL, 0
	breq secondTest
notNull:
	lpm XL, Z+
	lpm XH, Z+
	lpm r21, Z+
	push r21
	lpm r21, Z+
	push r21
	movw Z, X	
	rcall finder
	pop r19
	pop r20
	;max comparison
	cp r19, XH
	brlt test2
	cp r20, XL
	brlo test2
	mov XL, r19
	mov XH, r20
test2:
	cp YH, r19
	brlt return
	cp YL, r20
	brlo return
	mov YL, r19
	mov YH, r20
return:
	ret
secondTest:
 	cpi ZH, 0
 	brne notNull
	ldi XL, 0x00
	ldi XH, 0x80
	ldi Yl, 0xFF
	ldi YH, 0x7F
	ret

end:
	jmp end
