.include "m2560def.inc"

.set NEXT_STRING = 0x0000
.macro defstring ; str
	.set T = PC
	.dw NEXT_STRING << 1
	.set NEXT_STRING = T
	.if strlen(@0) & 1 ; odd length + null byte
		.db @0 , 0
	.else ; even length + null byte, add padding byte
		.db @0, 0, 0
	.endif
.endmacro


.cseg

jmp main
	defstring "this"
	defstring "macros"
	defstring "are"
	defstring "fun"

main:
	ldi ZL, LOW(NEXT_STRING <<1)
	ldi ZH, HIGH(NEXT_STRING <<1)
	rcall finder
	rjmp end

switch:
	mov r16, r19
	mov ZH, XH
	mov ZL, XL
	ret



finder:
	cpi ZL, 0
	breq secondTest
notNull:
	lpm XL, Z+
	lpm XH, Z+
	push ZL
	push ZH
	ldi r18, -1
	notLast:
	lpm r17, Z+
	inc r18
	cpi r17,0
	brne notLast
	push r18
	movw Z, X	
	rcall finder
	pop r19
	pop XH
	pop XL
	cp r16, r19
	brlt switch
	ret
secondTest:
 	cpi ZH, 0
 	brne notNull
	clr r16
	ret

end:
	jmp end
