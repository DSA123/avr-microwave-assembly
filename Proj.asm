.include "m2560def.inc"

.def powerPercent = r15
.def row = r16 ; current row number
.def col = r17 ; current column number
.def rmask = r30; mask for current row during scan
.def cmask = r29; mask for current column during scan
.def temp1 = r20
.def temp2 = r21
.def pressFlag = r24
.def mm = r23
.def ss = r22
.def toPrint = r19
.def read = r26
.def temp3 = r27
//.def mul10 = r25
.def previousMode = r25
.def currentMode = r18
.def doorMode = r31

.equ ENTRY = 0
.equ RUNNING = 1
.equ PAUSED = 2
.equ FINISHED = 3
.equ POWER = 4
.equ PORTLDIR = 0xF0 ; PD7-4: output, PD3-0, input
.equ INITCOLMASK = 0xEF ; scan from the rightmost column,
.equ INITROWMASK = 0x01 ; scan from the top row
.equ ROWMASK = 0x0F ; for obtaining input from Port D

.macro do_lcd_command
	push temp1
	ldi temp1, @0
	rcall lcd_command
	rcall lcd_wait
	pop temp1
.endmacro

.macro do_lcd_data
	push temp1
	mov temp1, @0
	rcall lcd_data
	rcall lcd_wait
	pop temp1
.endmacro

.macro clear
	ldi YL, low(@0)
	ldi YH, high(@0)
	push temp3
	clr temp3
	st Y+, temp3
	st Y, temp3
	pop temp3
.endmacro

.dseg
SecondCounter:
	.byte 2
TempCounter:
	.byte 2

.cseg
.org 0x0000
	jmp RESET
.org INT0addr
	jmp EXT_INT0
.org INT1addr
	jmp EXT_INT1
.org OVF0addr
	jmp Timer0OVF

RESET:
	ldi	temp1, low(RAMEND) ; initialize the stack
	out SPL, temp1
	ldi temp1, high(RAMEND)
	out SPH, temp1
	ldi temp1, PORTLDIR ; PA7:4/PA3:0, out/in
	sts DDRL, temp1
	ser temp1
	out DDRF, temp1
	out DDRA, temp1
	out DDRC, temp1
	out PORTC, temp1
	ldi temp3, 1
	clr temp1
	out PORTF, temp1
	out PORTA, temp1
	do_lcd_command 0b00111000 ; 2x5x7
	rcall sleep_5ms
	do_lcd_command 0b00111000 ; 2x5x7
	rcall sleep_1ms
	do_lcd_command 0b00111000 ; 2x5x7
	do_lcd_command 0b00111000 ; 2x5x7
	do_lcd_command 0b00001000 ; display off?
	do_lcd_command 0b00000001 ; clear display
	do_lcd_command 0b00000110 ; increment, no display shift
	do_lcd_command 0b00001110 ; Cursor on, bar, no blink
	clr pressFlag
	clear TempCounter
	clear SecondCounter
	ldi temp1, 0b00000000
	out TCCR0A, temp1
	ldi temp1, 0b00000010
	out TCCR0B, temp1
	ldi temp1, 1<<TOIE0
	sts TIMSK0, temp1
	clr temp1
	clr doorMode
	ldi temp1, (0b10<<ISC20)|(0b10<<ISC10)|(0b10<<ISC00)
	sts EICRA, temp1
	ldi temp1, (1<<INT2)|(1<<INT1)|(1<<INT0)
	out EIMSK, temp1
	
	ser temp1
	out DDRE, temp1 ; bit 3 is the OC3B
	clr temp1
	sts PORTE, temp1	

	ldi temp1, 0x00
	sts OCR3BL, temp1
	clr temp1
	sts OCR3BH, temp1

	ldi temp1, (1<<WGM32)|(1<<CS30)
	sts TCCR3B, temp1
	ldi temp1, (1<<WGM30)|(1<<COM3B1)
	sts TCCR3A, temp1
	push temp1
	ldi temp1, 0xFF
	mov powerPercent, temp1
	pop temp1
	sei
	jmp clearTime

Timer0OVF:
	push temp3
	in temp3, SREG
	push temp3
	push YH
	push YL
	push r25
	push r24

	lds r24, TempCounter
	lds r25, TempCounter+1
	adiw r25:r24, 1
	cpi r24, low(7812)
	ldi temp3, high(7812)
	cpc r25, temp3
	brne notSecondj
	cpi currentMode, RUNNING
	brne notRunning
	ser temp1

	sts OCR3BL, temp1
	clr temp1
	sts OCR3BH, temp1
	cpi ss, 0
	breq zeroSecs
	dec ss
	do_lcd_command 0b00000001
	rcall printTime
	lds  temp2, secondCounter
	rcall printPlate
	rcall printDoor
	jmp notRunning
notSecondj:
	jmp NotSecond
zeroSecs:
	cpi mm, 0
	brne notZeroMins
	ldi currentMode, FINISHED
	jmp printFinished
	;jmp EndIF //changed printFinished to jump to EndIF and we now jmp there instead of rcall printFinished
notZeroMins:
	dec mm
	ldi ss, 59
	do_lcd_command 0b00000001
	rcall printTime
	lds  temp2, secondCounter
	rcall printPlate
	rcall printDoor
	jmp notRunning
turnOffMotor:
	ldi temp1, 0x00
	sts OCR3BL, temp1
	clr temp1
	sts OCR3BH, temp1
	jmp EndIF
notRunning:
	clear TempCounter
	cpi currentMode, RUNNING
	brne turnOffMotor
	lds r24, SecondCounter
	lds r25, SecondCounter+1
	adiw r25:r24, 1
	sts SecondCounter, r24
	sts SecondCounter+1, r25
	rjmp EndIF
NotSecond:
	cpi r24, low(7812/4)
	ldi temp3, high(7812/4)
	cpc r25, temp3
	brsh quater
	jmp halfCheck
	//turns the motor off after 250ms of being on in power3
quater:
	mov temp1, powerPercent
	cpi temp1, 0x40
	brne halfcheck
	jmp turnOff
		//turns the motor off after 500ms of being on in power2
halfCheck:
	cpi r24, low(7812/2)
	ldi temp3, high(7812/2)
	cpc r25, temp3
	brlo keepOn
	cpi temp1, 0x80
	brne keepOn
turnOff:
	ldi temp1, 0x00
	sts OCR3BL, temp1
	sts OCR3BH, temp1
keepOn:
	sts TempCounter, r24
	sts TempCounter+1, r25
EndIF:
	pop r24
	pop r25
	pop YL
	pop YH
	pop temp3
	out SREG, temp3
	pop temp3
	reti

clearTime:
	clr read
	clr mm
	clr ss
;	out PORTC, read
	;do_lcd_command 0b00000001
	;rcall printTime
entryMode:
	ldi currentMode, ENTRY
	do_lcd_command 0b00000001
	rcall printTime
	rcall printPlate
	;do_lcd_command 0b1100000
	rcall printDoor
	jmp createmask

main:
	clr pressFlag
//input from keypad handling:
createmask:
	ldi	cmask, INITCOLMASK ; initial column mask
	clr col ; initial column
colloop:
	cpi col, 4
	breq main ; If all keys are scanned, repeat.
	sts PORTL, cmask ; Otherwise, scan a column.

	ldi temp1, 0xFF ; Slow down the scan operation.
delay:
	dec temp1
	brne delay

	lds temp1, PINL ; Read PORTA
	andi temp1, ROWMASK ; Get the keypad output value
	cpi temp1, 0xF ; Check if any row is low
	breq nextcol ; If yes, find which row is low

	ldi rmask, INITROWMASK ; Initialize for row check
	clr row
 
rowloop:
	cpi row, 4
	breq nextcol ; the row scan is over.
	mov temp2, temp1
	and temp2, rmask ; check un - masked bit
	breq convert ; if bit is clear, the key is pressed
	inc row ; else move to the next row
	lsl rmask
	jmp rowloop

nextcol:    ; if row scan is over
	lsl cmask
	inc col ; increase column value
	jmp colloop ; go to the next column

convert:
	cpi pressFlag, 1
	breq createmask
	ldi pressFlag, 1
	cpi doorMode, 1
	breq createmask
	cpi col, 3 ; If the pressed key is in col.3
	breq letters ; we have a letter

			 ; If the key is not in col.3 and

	cpi row, 3 ; If the key is in row3,
	brne notSym
	rjmp symbols ; we have a symbol or 0
notSym:
	mov temp1, row ; Otherwise we have a number in 1-9
	lsl temp1
	add temp1, row
	add temp1, col ; temp1 = row*3 + col
	subi temp1, -1 ; Add the value of character �1�
	jmp numbers

letters:
	ldi temp1, 'A'
	add temp1, row ; Get the ASCII value for the key
	cpi temp1, 'A'
	breq A
	cpi temp1, 'B'
	breq B
	cpi temp1, 'C'
	breq C
	cpi temp1, 'D'
	breq D

A:	cpi currentMode, ENTRY
	brne doNothing
	ldi currentMode, POWER
	do_lcd_command 0b00000001
	rcall printPower
	rcall printDoor
	jmp createmask

B:
	jmp createmask

	//adds 30 secs whilst running
C:	cpi currentMode, RUNNING
	brne doNothing
	cpi mm, 99
	breq convertSecs
	cpi ss, 60
	brsh convertSecs
	subi ss, -30
	cpi ss, 60
	brlo printSecs
	subi ss, 60
	subi mm, -1
printSecs:
	do_lcd_command 0b00000001
	rcall printTime
	rcall printPlate
	rcall printDoor
doNothing:
	jmp createmask
convertSecs:
	cpi mm, 99
	brlo doConvert
	jmp maxMins
doConvert:
	subi ss, 60
	subi mm, -1
	jmp C
maxMins:
	subi ss, -30
	cpi ss, 99
	brsh maxSecs
	jmp printSecs
maxSecs:
	ldi ss, 99
	jmp printSecs
	//subtracts 30sec while running
D:
	cpi currentMode, RUNNING
	brne doNothing
	cpi ss, 30
	brlo elimSecs
	subi ss, 30
	jmp printSecs
elimSecs:
	cpi mm, 0
	brne someMins
	ldi ss, 0
	jmp printSecs
someMins:
	subi mm, 1
	subi ss, -60
	subi ss, 30
	jmp printSecs

symbols:
	cpi col, 0 ; Check if we have a star
	breq star
	cpi col, 1 ; or if we have zero
	breq zero
	ldi temp1, '#' ; if not we have hash
	breq hash

hash:
	cpi currentMode, RUNNING
	breq pauseTime
	cpi currentMode, POWER
	brne doClear
	jmp entryMode
doClear:
	jmp clearTime
pauseTime:
	ldi currentMode, PAUSED
	jmp createmask

star:
	cpi currentMode, POWER
	breq doNothing
	cpi doorMode, 1
	breq doNothing
	cpi currentMode, RUNNING
	brne run
	inc mm
	cpi mm, 100
	breq make99
	jmp run
make99:
	ldi mm, 99

run:
	ldi currentMode, RUNNING
	cpi read, 0
	breq noTime
	jmp createmask
	noTime:
	ldi read, 4
	ldi mm, 1
	do_lcd_command 0b00000001
	rcall printTime
	rcall printPlate
	rcall printDoor
	jmp createmask

zero:
	cpi currentMode, FINISHED
	breq invalidInput
	cpi currentMode, POWER
	brne isntpower
invalidInput:
	jmp doNothing
isntpower:
	clr temp1
numbers:
	cpi currentMode, FINISHED
	breq invalidInput
	cpi currentMode, POWER
	brne NotPower
	jmp powerLevel
NotPower:
	cpi read, 4
	brlo cont
	jmp createmask
cont:
	inc read
;	out PORTC, read
	push temp2
	ldi temp2, 10
	mul mm, temp2
	pop temp2
;	ldi mul10, 10
;	mul mm, mul10
	mov mm, r0
	clr temp2

findHighSec:
	cpi ss, 10
	brlo MinAdd
	subi ss, 10
	inc temp2
	jmp findHighSec

MinAdd:
	add mm, temp2
	push temp2
	ldi temp2, 10
	mul ss, temp2
	pop temp2
	;mul ss, mul10
	mov ss, r0
	add ss, temp1
	mov r28, mm
	jmp entryMode
//prints mm:ss
printTime:
	push temp1
	push temp2
	clr temp2
	mov toPrint, mm
	rcall find_hundreds
	ldi temp1, ':'
	do_lcd_data temp1
	pop temp2
	pop temp1
	mov toPrint, ss
	push temp1
	push temp2
	clr temp2
	rcall find_hundreds
	pop temp2
	pop temp1
	ret

find_hundreds:
	cpi toPrint, 100
	brlo print_hundreds
	subi toPrint, 100
	inc temp2
	jmp find_hundreds
print_hundreds:
	clr temp2
find_tens:
	cpi toPrint,10
	brlo find_ones
	inc temp2
	subi toPrint, 10
	jmp find_tens
find_ones:
	ori temp2, 0x30
	do_lcd_data temp2
	clr temp2
	ori toPrint, 0x30
	do_lcd_data toPrint
	ret

Halt:
	jmp Halt
.equ LCD_RS = 7
.equ LCD_E = 6
.equ LCD_RW = 5
.equ LCD_BE = 4

.macro lcd_set
	sbi PORTA, @0
.endmacro
.macro lcd_clr
	cbi PORTA, @0
.endmacro

;
; Send a command to the LCD (r16)
;

lcd_command:
	push temp1
	out PORTF, temp1
	rcall sleep_1ms
	lcd_set LCD_E
	rcall sleep_1ms
	lcd_clr LCD_E
	rcall sleep_1ms
	pop temp1
	ret

lcd_data:
	push temp1
	out PORTF, temp1
	lcd_set LCD_RS
	rcall sleep_1ms
	lcd_set LCD_E
	rcall sleep_1ms
	lcd_clr LCD_E
	rcall sleep_1ms
	lcd_clr LCD_RS
	pop temp1
	ret

lcd_wait:
	push temp1
	clr temp1
	out DDRF, temp1
	out PORTF, temp1
	lcd_set LCD_RW
lcd_wait_loop:
	rcall sleep_1ms
	lcd_set LCD_E
	rcall sleep_1ms
	in temp1, PINF
	lcd_clr LCD_E
	sbrc temp1, 7
	rjmp lcd_wait_loop
	lcd_clr LCD_RW
	ser temp1
	out DDRF, temp1
	pop temp1
	ret

.equ F_CPU = 16000000
.equ DELAY_1MS = F_CPU / 4 / 1000 - 4
; 4 cycles per iteration - setup/call-return overhead

sleep_1ms:
	push r31
	push r30
	ldi r31, high(DELAY_1MS)
	ldi r30, low(DELAY_1MS)
delayloop_1ms:
	sbiw r31:r30, 1
	brne delayloop_1ms
	pop r31
	pop r30
	ret

sleep_5ms:
	rcall sleep_1ms
	rcall sleep_1ms
	rcall sleep_1ms
	rcall sleep_1ms
	rcall sleep_1ms
	ret
//prints current roatation of plate
printPlate:
	push temp2
	push temp1
padding:
do_lcd_command 0B10001111
sub20:
	cpi temp2, 20
	brlo switch
	subi temp2, 20
	jmp sub20
switch:
	cpi temp2,(5/2)
	brlo dash
	cpi temp2, 5
	brlo backSlash
	cpi temp2, (15/2)
	brlo pipe
	cpi temp2, 10
	brlo forwardSlash
	cpi temp2, (25/2)
	brlo dash
	cpi temp2, 15
	brlo backSlash
	cpi temp2, (35/2)
	brlo pipe
	cpi temp2, 20
	brlo forwardSlash
dash:
	ldi temp1, '-'
	jmp pPlate
backSlash:
	ldi temp1, 'A'
	jmp pPlate
pipe:
	ldi temp1, '|'
	jmp pPlate
forwardSlash:
	ldi temp1, '/'
pPlate:
	do_lcd_data temp1
	pop temp1
	pop temp2
	ret
//prints finished screen
printFinished:
	push temp1
	do_lcd_command 0b00000001
	ldi temp1, 'D'
	do_lcd_data temp1
	ldi temp1, 'o'
	do_lcd_data temp1
	ldi temp1, 'n'
	do_lcd_data temp1
	ldi temp1, 'e'
	do_lcd_data temp1
	rcall printPlate
	do_lcd_command 0b11000000
	ldi temp1, 'R'
	do_lcd_data temp1
	ldi temp1, 'e'
	do_lcd_data temp1
	ldi temp1, 'm'
	do_lcd_data temp1
	ldi temp1, 'o'
	do_lcd_data temp1
	ldi temp1, 'v'
	do_lcd_data temp1
	ldi temp1, 'e'
	do_lcd_data temp1
	ldi temp1, ' '
	do_lcd_data temp1
	ldi temp1, 'f'
	do_lcd_data temp1
	ldi temp1, 'o'
	do_lcd_data temp1
	ldi temp1, 'o'
	do_lcd_data temp1
	ldi temp1, 'd'
	do_lcd_data temp1
	rcall printDoor
	pop temp1
	jmp EndIF
//when in power level mode sets powerPercentage and displays the percentage on lcds
powerLevel:
	cpi temp1, 4
	brsh badInput
	mov temp3, temp1
	cpi temp3, 1
	breq power1
	cpi temp3, 2
	breq power2
	cpi temp3, 3
	breq power3
power1:
	ldi temp1, 0xff
	out PORTC, temp1
	mov powerPercent, temp1
	jmp loaded
power2:
	ldi temp1, 0x0f
	out PORTC, temp1
	ldi temp1, 0x80
	mov powerPercent, temp1
	push temp1
	jmp loaded
power3:
	ldi temp1, 0x3
	out PORTC, temp1
	ldi temp1, 0x40
	mov powerPercent, temp1
;	jmp loaded

loaded:
	jmp entryMode
badInput:
	pop temp1
	jmp doNothing
//prints power level
printPower:
	push temp1
	ldi temp1, 'S'
	do_lcd_data temp1
	ldi temp1, 'e'
	do_lcd_data temp1
	ldi temp1, 't'
	do_lcd_data temp1
	ldi temp1, ' '
	do_lcd_data temp1
	ldi temp1, 'P'
	do_lcd_data temp1
	ldi temp1, 'o'
	do_lcd_data temp1
	ldi temp1, 'w'
	do_lcd_data temp1
	ldi temp1, 'r'
	do_lcd_data temp1
	ldi temp1, ' '
	do_lcd_data temp1
	ldi temp1, '1'
	do_lcd_data temp1
	ldi temp1, '/'
	do_lcd_data temp1
	ldi temp1, '2'
	do_lcd_data temp1
	ldi temp1, '/'
	do_lcd_data temp1
	ldi temp1, '3'
	do_lcd_data temp1
	pop temp1
	do_lcd_command 0b11000000
	ret
//prints current status of door
printDoor:
	push temp1
	push 	doorMode
	do_lcd_command 0B11001111
	pop doorMode
	push doorMode
	cpi doorMode, 0
	breq printClosed
	cpi doorMode, 1
	breq printOpen
printClosed:
	ldi temp1, 'C'
	jmp finishedDoor
printOpen:
	ldi temp1, 'O'
finishedDoor:
	do_lcd_data temp1
	pop doorMode
	pop temp1
	ret
	//closes door
EXT_INT0:	//closed
	push temp1
	ldi temp1, 0x00
	out PORTE, temp1
	pop temp1
	cpi doorMode, 0
	breq return
	clr doorMode
	cpi currentMode, POWER
	breq wasPower
	cpi currentMode, FINISHED
	breq return
	cpi previousMode, ENTRY
	breq changeBack
	jmp dontChange
changeBack:
	ldi currentMode, ENTRY
dontChange:
	clr doorMode
	do_lcd_command 0b00000001
	rcall printTime
	rcall printPlate
	;do_lcd_command 0b11000000
    rcall printDoor
return:
	reti
	//opens door
EXT_INT1: // open
	cpi doorMode, 1
	breq return
	push temp1
	ldi temp1, 0xFF
	out PORTE, temp1
	pop temp1
	ldi doorMode, 1
	cpi currentMode, POWER
	breq wasPower
	cpi currentMode, FINISHED
	breq doEntry
	cpi currentMode, ENTRY
	breq wasEntry
	jmp wasntEntry
wasEntry:
	ldi previousMode, ENTRY
	jmp wasntRunning
wasntEntry:
	ldi previousMode, RUNNING
wasntRunning:
	ldi doorMode, 1
	ldi currentMode, PAUSED
	do_lcd_command 0b00000001
	rcall printTime
	rcall printPlate
	rcall printDoor
	jmp return
doEntry:
	ldi currentMode, ENTRY
	ldi doorMode, 1
	clr read
	do_lcd_command 0b00000001
	rcall printTime
	;do_lcd_command 0b11000000
	rcall printDoor
	jmp return
wasPower:
	do_lcd_command 0b00000001
	rcall printPower
	rcall printPlate
	rcall printDoor
	jmp return

