# AVR Assembly COMP2121 - Microprocessors #

This repo contains all the code for the COMP2121 labs as well as the final project. The labs each focus on an individual component of the microprocessor (i.e. LED display manipulation) and then combine together in the final project for the creation of a microwave via a microprocessor and a chip with a bunch of components (LED screen, buttons, motor etc.)

See the Proj.asm file for the microwave assembly code.

Enjoy!